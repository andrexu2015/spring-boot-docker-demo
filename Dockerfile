#FROM openjdk:8-jdk-alpine
FROM java
VOLUME /tmp
#把spring-boot-docker-test.jar和apiclient_key.pem 放在容器中
COPY ./target/spring-boot-docker-test.jar spring-boot-docker-test.jar
COPY ./wxpay/apiclient_key.pem apiclient_key.pem
COPY ./heapDump.sh /www/heapDump.sh
#ENV JAVA_OPTS="-Xms256M -Xmx256M -XX:+HeapDumpOnOutOfMemoryError"
#挂载/www/dump/
VOLUME ["/www/dump/"]
#ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/spring-boot-docker-test.jar","--spring.profiles.active=dev","&"]
#ENTRYPOINT java ${JAVA_OPTS} -Djava.security.egd=file:/dev/./urandom -jar /spring-boot-docker-test.jar --spring.profiles.active=dev &
#ENTRYPOINT ["/bin/sh","-c","java $JAVA_OPTS -jar /spring-boot-docker-test.jar"]
#CMD java $JAVA_OPTS -jar /spring-boot-docker-test.jar --spring.profiles.active=dev &
#ENTRYPOINT ["/bin/sh", "-c", "java -jar /spring-boot-docker-test.jar"]
#定义jvm参数 指定dump文件位置
ENTRYPOINT ["java","-Xms100m","-Xmx100m","-XX:+HeapDumpOnOutOfMemoryError","-XX:HeapDumpPath=www/dump/","-XX:OnOutOfMemoryError='www/heapDump.sh'","-server","-jar","/spring-boot-docker-test.jar","--spring.profiles.active=dev"]
