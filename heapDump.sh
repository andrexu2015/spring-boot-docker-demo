datetime=$(date "+%Y%m%d%H%M%S")
# shellcheck disable=SC2006
hprofs=`find /www/dump -name '*.hprof'`
for tmprof in $hprofs
do
    mv $tmprof `echo "$tmprof.$datetime"`
done
