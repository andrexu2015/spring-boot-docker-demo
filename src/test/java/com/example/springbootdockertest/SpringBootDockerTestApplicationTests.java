package com.example.springbootdockertest;

import cn.hutool.core.date.TimeInterval;
import com.baomidou.mybatisplus.generator.FastAutoGenerator;
import com.baomidou.mybatisplus.generator.config.OutputFile;
import com.baomidou.mybatisplus.generator.engine.FreemarkerTemplateEngine;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Collections;
import java.util.stream.LongStream;

@SpringBootTest
class SpringBootDockerTestApplicationTests {

    @Test
    void contextLoads() {
        TimeInterval timeInterval = new TimeInterval();
        timeInterval.start();
        long start = System.currentTimeMillis();
        LongStream.rangeClosed(0, 100_0000_0000L).parallel().reduce(0,Long::sum);
        long end = System.currentTimeMillis();
        System.out.println("sum="+"时间："+(end-start));
        System.out.println(timeInterval.interval());
    }

    public static void main(String[] args) {
        FastAutoGenerator.create("jdbc:mysql://121.4.225.208:3306/wxpay?useUnicode=true&characterEncoding=UTF-8&serverTimezone=UTC&allowMultiQueries=true", "root", "root")
                .globalConfig(builder -> {
                    builder.author("crush") // 设置作者
                            // .enableSwagger() // 开启 swagger 模式
                            .commentDate("yyyy-MM-dd") // 设置注释日期的格式
                            .fileOverride() // 覆盖已生成文件
                            .outputDir("/Users/liguangcheng/IdeaProjects/spring-boot-docker-test/src/main/java"); // 指定输出目录
                })
                .packageConfig(builder -> {
                    builder.parent("com.example.springbootdockertest") // 设置父包名
// .moduleName("") // 设置父包模块名
                            .entity("entity.DO") // 设置实体类生成位置，DO不能小写，是关键字
                            .controller("controller.paymentInfo") // 设置实体类生成位置，DO不能小写，是关键字
                            .pathInfo(Collections.singletonMap(OutputFile.xml, "/Users/liguangcheng/IdeaProjects/spring-boot-docker-test/src/main/resources/mapper"));// 设置mapperXml生成路径
                })
                .strategyConfig(builder -> {
                    builder.addInclude("t_payment_info") // 设置需要生成的表名
                            .addTablePrefix("t_") // 设置过滤表前缀,在生成实体类时会将过滤的表前缀去掉
                            .entityBuilder()// 进入实体类生成配置
                            .enableLombok()// 开启lombok，默认false
                            .enableRemoveIsPrefix()// 设置将表中字段的is前缀删除
                            .enableTableFieldAnnotation()// 开启生成实体时生成字段注解
                            .formatFileName("%sDo")// 设置生成实体类的文件名后缀
                            .build()
                            .serviceBuilder()// 进入服务层生成配置
                            .formatServiceFileName("%sService")// 设置生成服务层的文件名后缀，不设置的话服务层首字母会添加一个I
                            .build();
                })
                .templateEngine(new FreemarkerTemplateEngine()) // 使用Freemarker引擎模板，默认的是Velocity引擎模板
                .execute();

    }

}
