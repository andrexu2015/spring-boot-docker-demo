package com.example.springbootdockertest.listener.kafka;

import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.springframework.kafka.support.ProducerListener;
import org.springframework.stereotype.Component;

/**
 * @Author liguangcheng
 * @Date 2021/10/7 5:56 下午
 * @Vision 1.0
 **/
@Component
@Slf4j
public class KafkaSendResultHandler implements ProducerListener {
    //@Override
    //public void onSuccess(ProducerRecord producerRecord, RecordMetadata recordMetadata) {
    //    log.info("listener发送消息成功");
    //}

    @Override
    public void onError(ProducerRecord producerRecord, RecordMetadata recordMetadata, Exception exception) {
        log.info("listener发送消息失败");
    }
}
