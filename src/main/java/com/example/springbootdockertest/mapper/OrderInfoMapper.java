package com.example.springbootdockertest.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.springbootdockertest.entity.DO.OrderInfoDo;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.ResultType;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.mapping.ResultSetType;
import org.apache.ibatis.session.ResultHandler;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author crush
 * @since 2022-04-03
 */
public interface OrderInfoMapper extends BaseMapper<OrderInfoDo> {

    OrderInfoDo getOrder();

    Integer saveBatch(@Param("list") List<OrderInfoDo> list);

    @Select("${sql}")
    @Options(resultSetType = ResultSetType.FORWARD_ONLY, fetchSize = 1000)
    @ResultType(OrderInfoDo.class)
    void dynamicSelectLargeData1(@Param("sql") String sql, ResultHandler<OrderInfoDo> handler);

}
