package com.example.springbootdockertest.mapper;

import com.example.springbootdockertest.entity.DO.PaymentInfoDo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author crush
 * @since 2022-04-03
 */
public interface PaymentInfoMapper extends BaseMapper<PaymentInfoDo> {

}
