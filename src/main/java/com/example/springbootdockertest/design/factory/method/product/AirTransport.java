package com.example.springbootdockertest.design.factory.method.product;

/**
 * 航空运输
 *
 * @Author liguangcheng
 * @Date 2021/10/20 10:25 上午
 * @Vision 1.0
 **/
public class AirTransport implements Ship {
    @Override
    public void receipt() {
        System.out.println("航空收货");
    }

    @Override
    public void ok() {
        System.out.println("航空交货");
    }
}
