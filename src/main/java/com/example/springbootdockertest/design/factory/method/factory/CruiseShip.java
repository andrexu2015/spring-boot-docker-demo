package com.example.springbootdockertest.design.factory.method.factory;

import com.example.springbootdockertest.design.factory.method.product.Ship;
import com.example.springbootdockertest.design.factory.method.product.WaterwayTransport;

/**
 * 邮轮
 *
 * @Author liguangcheng
 * @Date 2021/10/20 10:31 上午
 * @Vision 1.0
 **/
public class CruiseShip extends TransportationFactory {
    @Override
    public Ship createShip() {
        return new WaterwayTransport();
    }
}
