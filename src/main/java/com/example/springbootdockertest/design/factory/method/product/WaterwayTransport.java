package com.example.springbootdockertest.design.factory.method.product;

/**
 * 水路运输
 *
 * @Author liguangcheng
 * @Date 2021/10/20 10:27 上午
 * @Vision 1.0
 **/
public class WaterwayTransport implements Ship {
    @Override
    public void receipt() {
        System.out.println("水路收货");
    }

    @Override
    public void ok() {
        System.out.println("水路交货");
    }
}
