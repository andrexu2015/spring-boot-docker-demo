package com.example.springbootdockertest.design.factory.method.product;

/**
 * 运输
 *
 * @Author liguangcheng
 * @Date 2021/10/20 10:21 上午
 * @Vision 1.0
 **/
public interface Ship {
    public void receipt();

    public void ok();
}
