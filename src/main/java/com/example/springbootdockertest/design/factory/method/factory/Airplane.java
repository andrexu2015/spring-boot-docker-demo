package com.example.springbootdockertest.design.factory.method.factory;

import com.example.springbootdockertest.design.factory.method.product.AirTransport;
import com.example.springbootdockertest.design.factory.method.product.Ship;

/**
 * 飞机
 *
 * @Author liguangcheng
 * @Date 2021/10/20 10:30 上午
 * @Vision 1.0
 **/
public class Airplane extends TransportationFactory {
    @Override
    public Ship createShip() {
        return new AirTransport();
    }
}
