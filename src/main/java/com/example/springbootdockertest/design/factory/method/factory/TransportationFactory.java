package com.example.springbootdockertest.design.factory.method.factory;

import com.example.springbootdockertest.design.factory.method.product.Ship;

/**
 * 交通工具工厂
 *
 * @Author liguangcheng
 * @Date 2021/10/20 10:19 上午
 * @Vision 1.0
 **/
public abstract class TransportationFactory {
    public abstract Ship createShip();

    public void start() {
        Ship ship = createShip();
        ship.receipt();
        ship.ok();
    }
}
