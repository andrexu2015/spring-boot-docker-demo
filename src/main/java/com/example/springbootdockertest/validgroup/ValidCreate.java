package com.example.springbootdockertest.validgroup;

import javax.validation.groups.Default;

/**
 * 校验分组:新增
 *
 * @author : Leon
 * @date : 2020/8/20
 */
public interface ValidCreate extends Default {
}
