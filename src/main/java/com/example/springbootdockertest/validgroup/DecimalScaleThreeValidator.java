package com.example.springbootdockertest.validgroup;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.math.BigDecimal;
import java.util.Objects;

/**
 * 最长3位小数的数字校验
 *
 * @Author liguangcheng
 * @Date 2022/1/11 3:11 下午
 * @Vision 1.0
 **/
public class DecimalScaleThreeValidator implements ConstraintValidator<DecimalScaleThree, BigDecimal> {

    @Override
    public void initialize(DecimalScaleThree constraintAnnotation) {
        ConstraintValidator.super.initialize(constraintAnnotation);
    }

    @Override
    public boolean isValid(BigDecimal bigDecimal, ConstraintValidatorContext constraintValidatorContext) {
        if (Objects.nonNull(bigDecimal)) {
            return bigDecimal.scale() <= 3;
        }
        return true;
    }
}
