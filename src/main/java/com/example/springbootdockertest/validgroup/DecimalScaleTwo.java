package com.example.springbootdockertest.validgroup;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 最长2位小数的数字校验
 *
 * @Author liguangcheng
 * @Date 2022/1/11 3:09 下午
 * @Vision 1.0
 **/
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.ANNOTATION_TYPE, ElementType.CONSTRUCTOR, ElementType.PARAMETER, ElementType.TYPE_USE})
@Retention(RetentionPolicy.RUNTIME)
//通过DecimalScaleTwoValidator类实现注解的相关校验操作
@Constraint(validatedBy = DecimalScaleTwoValidator.class)
@Documented
public @interface DecimalScaleTwo {
    String message() default "最长2位小数的数字";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
