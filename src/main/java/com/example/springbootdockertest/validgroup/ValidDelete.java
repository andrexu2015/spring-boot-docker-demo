package com.example.springbootdockertest.validgroup;

import javax.validation.groups.Default;

/**
 * 校验分组:删除
 *
 * @author : Leon
 * @date : 2020/8/20
 */
public interface ValidDelete extends Default {
}
