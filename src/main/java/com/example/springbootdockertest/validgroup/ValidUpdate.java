package com.example.springbootdockertest.validgroup;

import javax.validation.groups.Default;

/**
 * 校验分组:更新
 *
 * @author : Leon
 * @date : 2020/8/20
 */
public interface ValidUpdate extends Default {
}
