package com.example.springbootdockertest.controller.kafka;

import cn.hutool.core.util.StrUtil;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

/**
 * listener测试
 *
 * @Author liguangcheng
 * @Date 2021/10/5 3:47 下午
 * @Vision 1.0
 **/
@Component
public class KafkaCustomerListenerTest {
    @Autowired
    private KafkaTemplate kafkaTemplate;

    //@KafkaListener(topics = "kafkaTest")
    public void listenerTest(ConsumerRecord<String, String> consumerRecord) {
        String value = consumerRecord.value();
        String key = consumerRecord.key();
        System.out.println(StrUtil.format("消费了key-->{},value-->{}", key, value));
        kafkaTemplate.send("kafkaTestResult", key, "处理完毕");
    }
}
