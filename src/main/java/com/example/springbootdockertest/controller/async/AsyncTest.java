package com.example.springbootdockertest.controller.async;

import com.googlecode.aviator.AviatorEvaluator;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Component;

import java.util.concurrent.Future;

/**
 * async测试
 *
 * @Author liguangcheng
 * @Date 2021/10/9 11:02 上午
 * @Vision 1.0
 **/
@Component
public class AsyncTest {
    @Async
    public Future<String> asyncMethodWithReturnType(String s) {
        System.out.println("Execute method asynchronously - "
                + Thread.currentThread().getName());
        try {
            Thread.sleep(2000);
            System.out.println(s);
            AviatorEvaluator.execute(s);//错误的情况
            return new AsyncResult<>("hello world !!!!");

        } catch (InterruptedException e) {
            //
        }
        return new AsyncResult<>(s);
    }

    @Async
    public void asyncMethodWithVoidType(String s) {
        System.out.println("Execute method asynchronously - "
                + Thread.currentThread().getName());
        try {
            Thread.sleep(2000);
            System.out.println(s);
            AviatorEvaluator.execute(s);//错误的情况

        } catch (InterruptedException e) {
            //
        }
    }

}
