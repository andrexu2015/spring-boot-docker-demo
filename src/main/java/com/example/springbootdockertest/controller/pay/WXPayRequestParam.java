package com.example.springbootdockertest.controller.pay;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 微信支付
 *
 * @Author liguangcheng
 * @Date 2021/12/8 9:34 上午
 * @Vision 1.0
 **/
@NoArgsConstructor
@Data
public class WXPayRequestParam {

    /**
     * 直连商户的商户号，由微信支付生成并下发。
     */
    private String mchid;
    /**
     * 商户系统内部订单号，
     * 只能是数字、大小写字母_-*且在同一个商户号下唯一
     */
    private String out_trade_no;
    /**
     * 由微信生成的应用ID，
     * 全局唯一。请求基础下单接口时请注意APPID的应用属性，
     * 例如公众号场景下，需使用应用属性为公众号的APPID
     */
    private String appid;
    /**
     * 商品描述
     */
    private String description;
    /**
     * 异步接收微信支付结果通知的回调地址，
     * 通知url必须为外网可访问的url，不能携带参数。
     * 公网域名必须为https，如果是走专线接入，使用专线NAT IP或者私有回调域名可使用http
     */
    private String notify_url;
    /**
     * 订单金额信息
     */
    private Amount amount;
    /**
     * 支付者信息
     */
    private Payer payer;

    /**
     * 附加数据
     */
    private String attach;

    /**
     * Amount
     */
    @NoArgsConstructor
    @Getter
    @Setter
    public static class Amount {
        /**
         * 订单总金额，单位为分
         */
        private Integer total;
        /**
         * CNY：人民币，境内商户号仅支持人民币。
         */
        private String currency = "CNY";
    }

    /**
     * Payer
     */
    @NoArgsConstructor
    @Getter
    @Setter
    public static class Payer {
        /**
         * 用户在直连商户appid下的唯一标识。 下单前需获取到用户的Openid
         */
        private String openid;
    }
}
