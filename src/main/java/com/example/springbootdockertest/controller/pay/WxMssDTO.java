package com.example.springbootdockertest.controller.pay;

import lombok.Getter;
import lombok.Setter;

import java.util.List;
import java.util.Map;

/**
 * 小程序推送所需数据
 *
 * @Author liguangcheng
 * @Date 2022/5/25 10:59
 * @Vision 1.0
 **/
@Getter
@Setter
public class WxMssDTO {
    /**
     * 用户openid
     */
    private String touser;
    /**
     * 模版id
     */
    private String template_id;
    /**
     * 跳转到小程序页面路径
     */
    private String page;
    /**
     * 收集到的用户formid
     */
    private String form_id;
    /**
     * 放大那个推送字段
     */
    private String emphasis_keyword = "keyword1.DATA";
    private String miniprogram_state = "formal";
    private Map<String, TemplateData> data;//推送文字
    private List<String> testList;

    @Getter
    @Setter
    public static class TemplateData {
        //keyword1：订单类型，keyword2：下单金额，keyword3：配送地址，keyword4：取件地址，keyword5备注
        private String value;//,,依次排下去
    }
}
