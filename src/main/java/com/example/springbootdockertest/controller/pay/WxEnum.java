package com.example.springbootdockertest.controller.pay;

import lombok.AllArgsConstructor;
import lombok.Getter;


/**
 * 微信枚举
 *
 * @Author liguangcheng
 * @Date 2021/12/8 9:34 上午
 * @Vision 1.0
 **/
@AllArgsConstructor
@Getter
public enum WxEnum {
	/**
	 * JSAPI下单
	 */
	JSAPI_PAY("/v3/pay/transactions/jsapi"),

	/**
	 * Native下单
	 */
	NATIVE_PAY("/v3/pay/transactions/native"),

	/**
	 * Native下单
	 */
	NATIVE_PAY_V2("/pay/unifiedorder"),

	/**
	 * 查询订单
	 */
	ORDER_QUERY_BY_NO("/v3/pay/transactions/out-trade-no/%s"),

	/**
	 * 关闭订单
	 */
	CLOSE_ORDER_BY_NO("/v3/pay/transactions/out-trade-no/%s/close"),

	/**
	 * 申请退款
	 */
	DOMESTIC_REFUNDS("/v3/refund/domestic/refunds"),

	/**
	 * 查询单笔退款
	 */
	DOMESTIC_REFUNDS_QUERY("/v3/refund/domestic/refunds/%s"),

	/**
	 * 申请交易账单
	 */
	TRADE_BILLS("/v3/bill/tradebill"),

	/**
	 * 申请资金账单
	 */
	FUND_FLOW_BILLS("/v3/bill/fundflowbill"),

	/**
	 * 支付通知
	 */
	NATIVE_NOTIFY("/api/wx-pay/native/notify"),

	/**
	 * 支付通知
	 */
	NATIVE_NOTIFY_V2("/api/wx-pay-v2/native/notify"),

	/**
	 * JSAPI支付通知
	 */
	JSAPI_NOTIFY("/wechat/jsapi/notify"),


	/**
	 * 退款结果通知
	 */
	REFUND_NOTIFY("/api/wx-pay/refunds/notify"),

	/**
	 * 成功
	 */
	SUCCESS("SUCCESS"),

	/**
	 * 关闭
	 */
	CLOSED("CLOSED"),

	/**
	 * 退款处理中
	 */
	PROCESSING("PROCESSING"),

	/**
	 * 退款异常
	 */
	ABNORMAL("ABNORMAL"),

	/**
	 * 未支付
	 */
	NOTPAY("NOTPAY"),

	WXPAY("WECHAT"),
	/**
	 * 转入退款
	 */
	REFUND("REFUND");


	/**
	 * 类型
	 */
	private final String type;
}
