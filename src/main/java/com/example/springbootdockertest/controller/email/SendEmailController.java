package com.example.springbootdockertest.controller.email;

import com.example.springbootdockertest.entity.DO.MailSendDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileUrlResource;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.mail.internet.MimeMessage;
import java.net.URL;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * 发送邮件
 *
 * @Author liguangcheng
 * @Date 2022/4/22 15:54
 * @Vision 1.0
 **/
@RestController
@Slf4j
public class SendEmailController {
    @Autowired
    private JavaMailSender javaMailSender;
    private static final String FROM = "no-reply@kec-app.com";

    @PostMapping("/sendEmail")
    public void sendEmail() {

        MailSendDto mailSendDto = new MailSendDto();
        mailSendDto.setUserName("leon");
        mailSendDto.setAddress("lgccrush@163.com");

        String data = "<table dir=\"ltr\" class=\"body\" style=\"border-spacing:0;border-collapse:collapse;vertical-align:top;hyphens:none;-moz-hyphens:none;-webkit-hyphens:none;-ms-hyphens:none;background:#f3f3f3;height:100%;width:100%;color:#0a0a0a;font-family:'Cereal', Helvetica, Arial, sans-serif;font-weight:normal;padding:0;margin:0;text-align:left;font-size:16px;line-height:19px;margin-bottom:0px !important;background-color: white\"><tbody><tr style=\"padding:0;vertical-align:top;text-align:left\"><td class=\"center\" align=\"center\" valign=\"top\" style=\"word-wrap:break-word;-webkit-hyphens:auto;-moz-hyphens:auto;hyphens:auto;vertical-align:top;color:#0a0a0a;font-family:'Cereal', Helvetica, Arial, sans-serif;font-weight:normal;padding:0;margin:0;text-align:left;font-size:16px;line-height:19px;border-collapse:collapse !important\"><center style=\"width:100%;min-width:580px\"><table class=\"container\" style=\"border-spacing:0;border-collapse:collapse;padding:0;vertical-align:top;background:#fefefe;width:580px;margin:0 auto;text-align:inherit;max-width:580px;\"><tbody><tr style=\"padding:0;vertical-align:top;text-align:left\"><td style=\"word-wrap:break-word;-webkit-hyphens:auto;-moz-hyphens:auto;hyphens:auto;vertical-align:top;color:#0a0a0a;font-family:'Cereal', Helvetica, Arial, sans-serif;font-weight:normal;padding:0;margin:0;text-align:left;font-size:16px;line-height:19px;border-collapse:collapse !important\"><div><table class=\"row\" style=\"border-spacing:0;border-collapse:collapse;vertical-align:top;text-align:left;padding:0;width:100%;position:relative;display:table\"><tbody><tr class=\"\" style=\"padding:0;vertical-align:top;text-align:left\"><th class=\"columns first large-12 last small-12\" style=\"color:#0a0a0a;font-family:'Cereal', Helvetica, Arial, sans-serif;font-weight:normal;padding:0;text-align:left;font-size:16px;line-height:19px;margin:0 auto;padding-bottom:16px;width:564px;padding-left:16px;padding-right:16px\"><a href=\"https://www.airbnb.cn?eal_exp=1590045774&amp;eal_sig=2fb0d6b3f19866c973d224865b27119f3d10ed2c5d19c38cf8eb51a01610d1ba&amp;eal_uid=148115848&amp;eluid=0&amp;euid=942332bd-873d-a29f-3aae-7d162f63e328\" style=\"font-family:'Cereal', Helvetica, Arial, sans-serif;font-weight:normal;padding:0;margin:0;text-align:left;line-height:1.3;color:#2199e8;text-decoration:none\" rel=\"noopener\" target=\"_blank\"><img align=\"center\" class=\"center standard-header\" height=\"30\" " +
//      "src=\"https://a0.muscache.com/airbnb/rookery/dls/logo_standard_cn_2x-7767bd0474ee04b6a98e8d98568a75ce.png\"" +
                " style=\"outline:none;text-decoration:none;-ms-interpolation-mode:bicubic;width:auto;max-width:100%;clear:both;display:block;border:none;padding-bottom:16px;padding-top:48px;max-height:30px\"></a></th></tr></tbody></table></div><div><table class=\"row\" style=\"border-spacing:0;border-collapse:collapse;vertical-align:top;text-align:left;padding:0;width:100%;position:relative;display:table\"><tbody><tr class=\"\" style=\"padding:0;vertical-align:top;text-align:left\"><th class=\"columns first large-12 last small-12\" style=\"color:#0a0a0a;font-family:'Cereal', Helvetica, Arial, sans-serif;font-weight:normal;padding:0;text-align:left;font-size:16px;line-height:19px;margin:0 auto;padding-bottom:16px;width:564px;padding-left:16px;padding-right:16px\"><p class=\"body  body-lg  body-link-rausch light text-left   \" style=\"padding:0;margin:0;font-family:&quot;Cereal&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;font-weight:300;color:#484848;hyphens:none;-moz-hyphens:none;-webkit-hyphens:none;-ms-hyphens:none;font-size:18px;line-height:1.4;text-align:left;margin-bottom:0px !important;\">" +
                "嘉宁" +
                "，您好：\n" +
                "      </p></th></tr></tbody></table></div><div><table class=\"row\" style=\"border-spacing:0;border-collapse:collapse;vertical-align:top;text-align:left;padding:0;width:100%;position:relative;display:table\"><tbody><tr class=\"\" style=\"padding:0;vertical-align:top;text-align:left\"><th class=\"columns first large-12 last small-12\" style=\"color:#0a0a0a;font-family:'Cereal', Helvetica, Arial, sans-serif;font-weight:normal;padding:0;text-align:left;font-size:16px;line-height:19px;margin:0 auto;padding-bottom:16px;width:564px;padding-left:16px;padding-right:16px\"><p class=\"body  body-lg  body-link-rausch light text-left   \" style=\"padding:0;margin:0;font-family:&quot;Cereal&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;font-weight:300;color:#484848;hyphens:none;-moz-hyphens:none;-webkit-hyphens:none;-ms-hyphens:none;font-size:18px;line-height:1.4;text-align:left;margin-bottom:0px !important;\">我们收到了您重置密码的申请。\n" +
                "      </p></th></tr></tbody></table></div><div><table class=\"row\" style=\"border-spacing:0;border-collapse:collapse;vertical-align:top;text-align:left;padding:0;width:100%;position:relative;display:table\"><tbody><tr class=\"\" style=\"padding:0;vertical-align:top;text-align:left\"><th class=\"columns first large-12 last small-12\" style=\"color:#0a0a0a;font-family:'Cereal', Helvetica, Arial, sans-serif;font-weight:normal;padding:0;text-align:left;font-size:16px;line-height:19px;margin:0 auto;padding-bottom:16px;width:564px;padding-left:16px;padding-right:16px\"><p class=\"body  body-lg  body-link-rausch light text-left   \" style=\"padding:0;margin:0;font-family:&quot;Cereal&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;font-weight:300;color:#484848;hyphens:none;-moz-hyphens:none;-webkit-hyphens:none;-ms-hyphens:none;font-size:18px;line-height:1.4;text-align:left;margin-bottom:0px !important;\">如果您没有提出此请求，请忽略此消息。 否则，请重新设置密码。\n" +
                "      </p></th></tr></tbody></table></div><div><table class=\"row\" style=\"border-spacing:0;border-collapse:collapse;vertical-align:top;text-align:left;padding:0;width:100%;position:relative;display:table\"><tbody><tr style=\"padding:0;vertical-align:top;text-align:left\"><th class=\"col-pad-left-2 col-pad-right-2\" style=\"color:#0a0a0a;font-family:'Cereal', Helvetica, Arial, sans-serif;font-weight:normal;padding:0;margin:0;text-align:left;font-size:16px;line-height:19px;padding-left:16px;padding-right:16px\"><a href=\"https://www.airbnb.cn/users/set_password?c=.pi80.pkYm9va2luZy9hdXRoZW50aWNhdGlvbi9yZXNldF9wYXNzd29yZA%3D%3D&amp;euid=942332bd-873d-a29f-3aae-7d162f63e328&amp;secret=MOrZAbeo03e8kJzzWjTMXdcypdGQ8cAtJIEhNBsJfyFZasVvT5-2HX0G9JDzjKXVHum_iR0Lfy8AR3qtM5XQlw\" class=\"btn-primary btn-md btn-rausch\" style=\"font-family:'Cereal', Helvetica, Arial, sans-serif;font-weight:normal;margin:0;text-align:left;line-height:1.3;color:#2199e8;text-decoration:none;background-color:#ff5a5f;-webkit-border-radius:4px;border-radius:4px;display:inline-block;padding:12px 24px 12px 24px;\" rel=\"noopener\" target=\"_blank\"><p class=\"text-center\" style=\"font-weight:normal;padding:0;margin:0;text-align:center;font-family:&quot;Cereal&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;color:white;font-size:18px;line-height:26px;margin-bottom:0px !important;\">重置密码\n" +
                "        </p></a></th></tr></tbody></table></div><div style=\"padding-top:24px\"><table class=\"row\" style=\"border-spacing:0;border-collapse:collapse;vertical-align:top;text-align:left;padding:0;width:100%;position:relative;display:table\"><tbody><tr class=\"\" style=\"padding:0;vertical-align:top;text-align:left\"><th class=\"columns first large-12 last small-12\" style=\"color:#0a0a0a;font-family:'Cereal', Helvetica, Arial, sans-serif;font-weight:normal;padding:0;text-align:left;font-size:16px;line-height:19px;margin:0 auto;padding-bottom:16px;width:564px;padding-left:16px;padding-right:16px\"><p class=\"body  body-lg  body-link-rausch light text-left   \" style=\"padding:0;margin:0;font-family:&quot;Cereal&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;font-weight:300;color:#484848;hyphens:none;-moz-hyphens:none;-webkit-hyphens:none;-ms-hyphens:none;font-size:18px;line-height:1.4;text-align:left;margin-bottom:0px !important;\">谢谢！<br>爱彼迎团队      </p></th></tr></tbody></table></div><div style=\"padding-top:56px\"><table class=\"row\" style=\"border-spacing:0;border-collapse:collapse;vertical-align:top;text-align:left;padding:0;width:100%;position:relative;display:table\"><tbody><tr class=\"\" style=\"padding:0;vertical-align:top;text-align:left\"><th class=\"columns first large-12 last small-12 standard-footer-padding\" style=\"color:#0a0a0a;font-family:'Cereal', Helvetica, Arial, sans-serif;font-weight:normal;padding:0;text-align:left;font-size:16px;line-height:19px;margin:0 auto;padding-bottom:16px;width:564px;padding-left:16px;padding-right:16px\"><div class=\"\"><hr class=\"standard-footer-hr\" style=\"max-width:580px;border-right:0;border-top:0;border-bottom:1px solid #cacaca;border-left:0;clear:both;background-color:#dbdbdb;height:2px;width:100%;border:none;margin:auto\"><div class=\"row-pad-bot-4\" style=\"padding-bottom:32px\"></div><p class=\"standard-footer-text \" style=\"padding:0;margin:0;text-align:left;margin-bottom:10px;font-family:&quot;Cereal&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;color:#9ca299;font-size:14px;text-shadow:0 1px #fff;font-weight:300;line-height:1.4\"></p><p class=\"standard-footer-text center \" style=\"padding:0;margin:0;text-align:left;margin-bottom:10px;font-family:&quot;Cereal&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;color:#9ca299;font-size:14px;text-shadow:0 1px #fff;font-weight:300;line-height:1.4\">由爱彼迎总部发送 ♥\n" +
                "        </p><p class=\"standard-footer-text \" style=\"padding:0;margin:0;text-align:left;margin-bottom:10px;font-family:&quot;Cereal&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;color:#9ca299;font-size:14px;text-shadow:0 1px #fff;font-weight:300;line-height:1.4\">&zwnj;A&zwnj;i&zwnj;r&zwnj;b&zwnj;n&zwnj;b&zwnj;,&zwnj; &zwnj;I&zwnj;n&zwnj;c&zwnj;.&zwnj;,&zwnj; &zwnj;8&zwnj;8&zwnj;8&zwnj; &zwnj;B&zwnj;r&zwnj;a&zwnj;n&zwnj;n&zwnj;a&zwnj;n&zwnj; &zwnj;S&zwnj;t&zwnj;,&zwnj; &zwnj;S&zwnj;a&zwnj;n&zwnj; &zwnj;F&zwnj;r&zwnj;a&zwnj;n&zwnj;c&zwnj;i&zwnj;s&zwnj;c&zwnj;o&zwnj;,&zwnj; &zwnj;C&zwnj;A&zwnj; &zwnj;9&zwnj;4&zwnj;1&zwnj;0&zwnj;3&zwnj;        </p><p class=\"standard-footer-text \" style=\"padding:0;margin:0;text-align:left;margin-bottom:10px;font-family:&quot;Cereal&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;color:#9ca299;font-size:14px;text-shadow:0 1px #fff;font-weight:300;line-height:1.4\"><a class=\"muted\" href=\"https://www.airbnb.cn/invite?eal_exp=1590045774&amp;eal_sig=2fb0d6b3f19866c973d224865b27119f3d10ed2c5d19c38cf8eb51a01610d1ba&amp;eal_uid=148115848&amp;eluid=1&amp;euid=942332bd-873d-a29f-3aae-7d162f63e328&amp;r=46\" style=\"font-family:'Cereal', Helvetica, Arial, sans-serif;font-weight:normal;padding:0;margin:0;text-align:left;line-height:1.3;color:#9ca299;text-decoration:underline\" rel=\"noopener\" target=\"_blank\">赚取旅行基金\n" +
                "          </a></p><p class=\"standard-footer-text \" style=\"padding:0;margin:0;text-align:left;margin-bottom:10px;font-family:&quot;Cereal&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;color:#9ca299;font-size:14px;text-shadow:0 1px #fff;font-weight:300;line-height:1.4\"></p></div></th></tr></tbody></table></div></td></tr></tbody></table></center></td></tr></tbody></table>";
        mailSendDto.setData(data);
        mailSendDto.setTitle("测试12");
        this.sendMail(mailSendDto);
    }

    public String sendMail(MailSendDto mailSendDto) {
        try {
            //不带附件
            SimpleMailMessage message = new SimpleMailMessage();
            message.setTo(mailSendDto.getAddress());
            message.setSubject(mailSendDto.getTitle());
            message.setText(mailSendDto.getData());
            message.setCc("lgccrush@163.com");
            message.setFrom(FROM);
            MimeMessage mimeMessage = javaMailSender.createMimeMessage();
            // 带附件
            MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true, "UTF-8");
            helper.setFrom(FROM);
            helper.setTo(mailSendDto.getAddress());
            helper.setCc("lgccrush@163qq.com");
            helper.setSubject(mailSendDto.getTitle());
            helper.setText(mailSendDto.getData());

            HashMap<String, String> filePaths = new HashMap<>();//改变文件下载名称
            filePaths.put("测试excel", "http://8.129.211.119:8090/group1/M00/91/03/rBd85GJhHW6ALh2wAAAkmOO32IA42.xlsx");
            try {
                if (filePaths != null) {
                    Iterator var8 = filePaths.entrySet().iterator();

                    while (var8.hasNext()) {
                        Map.Entry<String, String> entry = (Map.Entry) var8.next();
                        URL url = new URL((String) entry.getValue());
                        FileUrlResource fileUrlResource = new FileUrlResource(url);
                        helper.addAttachment((String) entry.getKey(), fileUrlResource);
                    }
                }
            } catch (Exception var12) {
                var12.printStackTrace();
            }

            javaMailSender.send(message);
            javaMailSender.send(helper.getMimeMessage());
            log.info("邮件发送成功");
            return "邮件发送成功";
        } catch (Exception e) {
            log.error("邮件:发送失败", e);
            return "邮件发送失败";
        }

    }
}
