package com.example.springbootdockertest.controller.validate;

import com.example.springbootdockertest.validgroup.DecimalScaleThree;
import com.example.springbootdockertest.validgroup.ValidCreate;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.*;
import java.math.BigDecimal;


@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class SortCodeImportDto {


    @NotNull(message = "拦截标记,不能为空!", groups = {ValidCreate.class})
    private String isInterceptName;

    /**
     * 拦截信息提示
     */
    private String interceptInfo;

    /**
     * 分拣码名称
     */
    @NotBlank(message = "分拣码名称,不能为空!", groups = {ValidCreate.class})
    private String sortCodeName;

    /**
     * 分拣码
     */
    @NotBlank(message = "分拣码,不能为空!", groups = {ValidCreate.class})
    private String sortCode;

    /**
     * 优先级
     */
    @Max(10000)
    @Min(0)
    @NotNull(message = "优先级,正整数,必填!", groups = {ValidCreate.class})
    private Integer priority;

    /**
     * 指定路线
     */
    @NotNull(message = "指定路线,不能为空!", groups = {ValidCreate.class})
    private String hubInNames;

    /**
     * 收费重开始
     */
    @DecimalMin(value = "0", message = "收费重,申报价值,COD金额不允许为负数.")
    @DecimalScaleThree(message = "收费重最长3位小数的数字")
    private BigDecimal chargeWeightBegin;


    /**
     * 收费重结束
     */
    @DecimalMin(value = "0", message = "收费重,申报价值,COD金额不允许为负数.")
    @DecimalScaleThree(message = "收费重最长3位小数的数字")
    private BigDecimal chargeWeightEnd;

}
