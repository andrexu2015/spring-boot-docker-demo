package com.example.springbootdockertest.controller.validate;

import com.example.springbootdockertest.validgroup.ValidCreate;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * validate测试
 *
 * @Author liguangcheng
 * @Date 2022/1/16 5:28 下午
 * @Vision 1.0
 **/
@RestController
public class ValidateController {

    @PostMapping("validate")
    public void validate(@Validated({ValidCreate.class}) @RequestBody SortCodeImportDto dto) {
        System.out.println(dto);
    }

    public static void main(String[] args) {
        SortCodeImportDto importDto = new SortCodeImportDto();
        try {
            ValidatorUtils.validate(importDto, ValidCreate.class);
        } catch (Exception e) {
            System.out.println(e);
        }
    }
}
