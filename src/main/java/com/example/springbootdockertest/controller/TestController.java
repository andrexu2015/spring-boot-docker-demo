package com.example.springbootdockertest.controller;

import cn.hutool.http.HttpRequest;
import cn.hutool.http.HttpResponse;
import cn.hutool.json.JSONUtil;
import com.example.springbootdockertest.controller.test.Sku;
import com.example.springbootdockertest.controller.test.SkuTest;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

/**
 * Test
 *
 * @Author liguangcheng
 * @Date 2021/10/12 1:45 下午
 * @Vision 1.0
 **/
@RestController
public class TestController {
    // @Value("${ship}")
    // private String shipConfig;

    @GetMapping("test")
    public String get() {
        Map<String, Object> json = new HashMap<>();
        Map<String, Object> text = new HashMap<>();
        text.put("content", "业务报警测试");
        json.put("msgtype", "text");
        json.put("text", text);
        String bodyStr = JSONUtil.toJsonStr(json);
        HttpResponse test = HttpRequest.post("https://oapi.dingtalk.com/robot/send?access_token=ccf16ddec6a9d4d63e180d060bf82dd1e205270090aff75d8e11b472c1c5ed26").body(bodyStr).execute();
        String body = test.body();

        return "hello springBoot Docker";
    }

    @PostMapping("testjson")
    public String test(@RequestBody String s) {
        System.out.println(s);

        return "hello springBoot Docker";
    }

    @GetMapping("test1")
    public String get4() {
        return "installation";
    }

    @GetMapping("test2")
    public void get2() {
        // Properties properties = new Properties();
        // String shipConfig = properties.getProperty("dubbo.provider.version");
        //
        // TransportationFactory factory;
        // if (Objects.equals("air", shipConfig)) {
        //     factory = new Airplane();
        // } else {
        //     factory = new CruiseShip();
        // }
        // factory.start();
    }

    @PostMapping("test3")
    public void get3(@RequestBody SkuTest test) {
        Sku sku = test.getSku();
    }


}
