package com.example.springbootdockertest.controller.excel;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;

/**
 * excel实体
 */
@Data
public class ExcelDemo {

  @ExcelProperty(index = 0)
  private String id;

  @ExcelProperty(index = 1)
  private String manifestId;


}
