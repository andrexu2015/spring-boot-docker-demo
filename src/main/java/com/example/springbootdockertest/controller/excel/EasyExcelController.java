package com.example.springbootdockertest.controller.excel;

import com.alibaba.excel.EasyExcel;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotNull;

/**
 * @Author liguangcheng
 * @Date 2022/1/26 1:33 下午
 * @Vision 1.0
 **/
@RestController
public class EasyExcelController {

    @PostMapping("/upload")
    public void uploadSingle(@RequestParam("file") MultipartFile file, @NotNull String token, @NotNull String platform) {

        try {
            EasyExcel.read(file.getInputStream(), ExcelDemo.class, new ExcelDemoListener(token, platform)).sheet().doRead();
        } catch (Exception e) {
            System.out.println(e);
        }
        return;
    }
}
