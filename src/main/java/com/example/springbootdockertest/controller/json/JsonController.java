package com.example.springbootdockertest.controller.json;

import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;

import java.util.concurrent.ThreadLocalRandom;

/**
 * JSON测试
 *
 * @Author liguangcheng
 * @Date 2022/1/15 5:10 下午
 * @Vision 1.0
 **/
public class JsonController {
    // 合并json
    public static void main(String[] args) {
        String jsonOne = "{ \"sys\": \"testSys\", \"testSign\": \"testSign\"}";
        String jsonTwo = "{ \"Code\": \"testCode\", \"sign\": \"sign\"}";
        JSONObject jsonObject = JSONUtil.parseObj(jsonOne);
        JSONObject extJsonObject = JSONUtil.parseObj(jsonTwo);
        JSONObject jsonThree = new JSONObject();
        jsonThree.putAll(jsonObject);
        jsonThree.putAll(extJsonObject);


        ThreadLocalRandom random = ThreadLocalRandom.current();
        int count = 0;
        for (int i = 0; i < 5; i++) {
            if (isInterested(random.nextInt(10))) {
                count++;
            }
        }
        System.out.printf("Found %d interested values%n", count);
    }

    private static boolean isInterested(int i) {
        String url = i + ".com";
        String test = i + ".com";
        return i % 2 == 0;

    }
}
