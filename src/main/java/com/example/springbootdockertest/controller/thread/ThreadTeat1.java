package com.example.springbootdockertest.controller.thread;

import java.util.concurrent.TimeUnit;

/**
 * 测试8锁
 *
 * @Author liguangcheng
 * @Date 2022/2/15 10:59 上午
 * @Vision 1.0
 **/
public class ThreadTeat1 {

    // 1、标准情况下，两个线程先打印 发短信还是 打电话？       1/发短信 2/打电话       1->2
    // 1、sendSms延迟4秒，两个线程先打印 发短信还是 打电话？  1/发短信 2/打电话       1->2
    public static void main(String[] args) {
        Phone phone = new Phone();
        //锁的存在
        new Thread(() -> {
            phone.sendSms();
        }, "A").start();
        // 捕获
        try {
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        new Thread(() -> {
            phone.call();
        }, "B").start();
    }
}

class Phone {
    // synchronized 锁的对象是方法的调用者！、 // 两个方法用的是同一个锁，谁先拿到谁执行！
    public synchronized void sendSms() {
        // try {
        //     TimeUnit.SECONDS.sleep(4);
        // } catch (InterruptedException e) {
        //     e.printStackTrace();
        // }
        System.out.println("发短信");
    }

    public synchronized void call() {
        System.out.println("打电话");
    }
}
