package com.example.springbootdockertest.controller.thread;

/**
 * 多线程测试
 *
 * @Author liguangcheng
 * @Date 2022/2/15 10:27 上午
 * @Vision 1.0
 **/
public class ThreadTestController {
    public static void main(String[] args) {
        // 并发：多线程操作同一个资源类, 把资源类丢入线程
        Ticket ticket = new Ticket();
        // @FunctionalInterface 函数式接口，jdk1.8 lambda表达式 (参数)->{ 代码 }
        new Thread(() -> {
            for (int i = 1; i < 4000; i++) {
                ticket.lockSale();
            }
        }, "A").start();
        new Thread(() -> {
            for (int i = 1; i < 4000; i++) {
                ticket.lockSale();
            }
        }, "B").start();
        new Thread(() -> {
            for (int i = 1; i < 4000; i++) {
                ticket.lockSale();
            }
        }, "C").start();
    }

}
