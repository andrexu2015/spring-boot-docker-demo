package com.example.springbootdockertest.controller.thread;

import java.util.concurrent.TimeUnit;

/**
 * 测试8锁
 *
 * @Author liguangcheng
 * @Date 2022/2/15 10:59 上午
 * @Vision 1.0
 **/
public class ThreadTeat2 {

    // 3、 增加了一个普通方法后！先执行发短信还是Hello？ 普通方法
    // 4、两个对象，两个同步方法， 发短信还是 打电话？ // 打电话->发短信
    public static void main(String[] args) {
        // 两个对象，两个调用者，两把锁！
        Phone2 phone1 = new Phone2();
        Phone2 phone2 = new Phone2();
        //锁的存在
        new Thread(() -> {
            phone1.sendSms();
        }, "A").start();
        // 捕获
        try {
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        new Thread(() -> {
            phone2.hello();
        }, "B").start();
        new Thread(() -> {
            phone2.call();
        }, "C").start();
    }
}

class Phone2 {
    // synchronized 锁的对象是方法的调用者！
    public synchronized void sendSms() {
        try {
            TimeUnit.SECONDS.sleep(4);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("发短信");
    }

    public synchronized void call() {
        System.out.println("打电话");
    }

    // 这里没有锁！不是同步方法，不受锁的影响
    public void hello() {
        System.out.println("hello");
    }
}
