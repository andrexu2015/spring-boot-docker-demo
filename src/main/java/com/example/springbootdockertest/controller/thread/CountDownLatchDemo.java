package com.example.springbootdockertest.controller.thread;

import java.util.concurrent.CountDownLatch;

/**
 * 允许一个或多个线程等待知道在其他线程中执行的一组操作完成的同步辅助
 *
 * @Author liguangcheng
 * @Date 2022/2/15 5:11 下午
 * @Vision 1.0
 **/
public class CountDownLatchDemo {
    public static void main(String[] args) throws InterruptedException { // 总数是6，必须要执行任务的时候，再使用！
        CountDownLatch countDownLatch = new CountDownLatch(50);
        for (int i = 1; i <= 50; i++) {
            new Thread(() -> {
                System.out.println(Thread.currentThread().getName() + " Go out");
                countDownLatch.countDown(); // 数量-1
            }, String.valueOf(i)).start();
        }
        countDownLatch.await(); // 等待计数器归零，然后再向下执行
        System.out.println("Close Door");
    }
}
