package com.example.springbootdockertest.controller.thread;

import java.util.concurrent.TimeUnit;

/**
 * 测试8锁
 *
 * @Author liguangcheng
 * @Date 2022/2/15 10:59 上午
 * @Vision 1.0
 **/
public class ThreadTeat4 {

    // * 5、增加两个静态的同步方法，只有一个对象，先打印 发短信？打电话？ 发短信->打电话
    // * 6、两个对象！增加两个静态的同步方法， 先打印 发短信？打电话？   发短信->打电话
    public static void main(String[] args) {
        // Phone3唯一的一个 Class 对象
        // 两个对象的Class类模板只有一个，static，锁的是Class
        Phone4 phone1 = new Phone4();
        Phone4 phone2 = new Phone4();
        // Phone3 phone2 = new Phone3();
        //锁的存在
        new Thread(() -> {
            phone1.sendSms();
        }, "A").start();
        // 捕获
        try {
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        new Thread(() -> {
            phone1.call();
        }, "C").start();
    }
}

class Phone4 {
    public static synchronized void sendSms() {
        try {
            TimeUnit.SECONDS.sleep(4);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("发短信");
    }

    public synchronized void call() {
        System.out.println("打电话");
    }
}
