package com.example.springbootdockertest.controller.xml;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * xml响应
 *
 * @Author liguangcheng
 * @Date 2022/1/15 2:39 下午
 * @Vision 1.0
 **/
@Getter
@Setter
@JsonRootName("response")//根节点
public class XMLResponse {
    @JsonProperty(value = "name")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String name;
    @JsonProperty(value = "age")
    private Integer age;
    @JsonProperty(value = "score")
    private String score;
    @JsonProperty(value = "xmlResposeList")
    @JacksonXmlElementWrapper(useWrapping = false)
    private List<XMLResponseList> xmlResponseLists;
}
