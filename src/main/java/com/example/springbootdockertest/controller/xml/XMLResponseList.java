package com.example.springbootdockertest.controller.xml;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * @Author liguangcheng
 * @Date 2022/1/15 2:41 下午
 * @Vision 1.0
 **/
@Getter
@Setter
public class XMLResponseList {
    @JsonProperty(value = "age")
    private String responseAge;
    @JsonProperty(value = "name")
    private String responseName;
}
