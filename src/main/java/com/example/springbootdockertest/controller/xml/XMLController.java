package com.example.springbootdockertest.controller.xml;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.google.common.collect.Lists;
import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.Objects;

/**
 * xml测试
 *
 * @Author liguangcheng
 * @Date 2022/1/15 2:37 下午
 * @Vision 1.0
 **/
@Controller
public class XMLController {
    //consumes:指定指定处理请求的提交内容类型
    //produces:指定返回的内容类型 produces = {"application/xml;charset=UTF-8"}返回xml格式
    @PostMapping(value = "/test/xml", consumes = {MediaType.APPLICATION_XML_VALUE, MediaType.TEXT_XML_VALUE}, produces = MediaType.APPLICATION_XML_VALUE)
    @ResponseBody
    public ResponseEntity testXML(@RequestBody String request) throws Exception {
        //解析xml请求数据
        Document requestDoc = DocumentHelper.parseText(request);
        Element requestRoot = requestDoc.getRootElement();// 指向根节点
        Iterator it = requestRoot.elementIterator();
        while (it.hasNext()) {
            Element element = (Element) it.next();// 一个Item节点
            String name = element.getName();
            if (Objects.equals("Validation", name)) {
                Element userIDElement = element.element("UserID");
                System.out.println("userId = " + userIDElement.getTextTrim());
                Element passwordElement = element.element("Password");
                System.out.println("password = " + passwordElement.getTextTrim());
            }
            if (Objects.equals("TrackingNumber", name)) {
                String trackingNumber = element.getTextTrim();
                System.out.println("TrackingNumber=" + trackingNumber);
            }
        }

        XMLResponse xmlResponse = new XMLResponse();
        // xmlResponse.setName("name");
        xmlResponse.setScore("score");
        xmlResponse.setAge(1);

        ArrayList<XMLResponseList> xmlResponseLists = Lists.newArrayList();
        XMLResponseList xmlResponseList = new XMLResponseList();
        xmlResponseList.setResponseAge("Age");
        xmlResponseList.setResponseName("name");
        xmlResponseLists.add(xmlResponseList);
        xmlResponseLists.add(xmlResponseList);
        xmlResponse.setXmlResponseLists(xmlResponseLists);

        XmlMapper xmlMapper = new XmlMapper();
        String response = xmlMapper.writeValueAsString(xmlResponse);
        Document responseDoc = DocumentHelper.parseText(response);
        Element rootElement = responseDoc.getRootElement();
        rootElement.addNamespace("xsi", "http://www.w3.org/2001/XMLSchema-instance");
        ResponseEntity stringResponseEntity = ResponseEntity.status(HttpStatus.OK).body(responseDoc.asXML());
        return stringResponseEntity;
    }

    public static void main(String[] args) {
        System.out.println(new Date(1645162725711l));
    }
}
