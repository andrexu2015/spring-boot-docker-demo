package com.example.springbootdockertest.controller.test;

import cn.hutool.core.date.DatePattern;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;

/**
 * 测试LocalDateTime
 *
 * @Author liguangcheng
 * @Date 2022/5/11 13:33
 * @Vision 1.0
 **/
public class LocalDateTimeTest {
    static DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DatePattern.NORM_DATETIME_PATTERN);

    public static void main(String[] args) {
        //JDK8 以前Calendar 获取指定时间
        Calendar today = Calendar.getInstance();
        today.set(Calendar.HOUR_OF_DAY, 8);
        today.set(Calendar.MINUTE, 00);
        today.set(Calendar.SECOND, 00);
        today.set(Calendar.MILLISECOND, 0);
        // String start = DateUtil.formatDateTime(DateUtil.offsetDay(today.getTime(), -1));
        // String end = DateUtil.formatDateTime(today.getTime());

        String start = LocalDateTime.of(LocalDate.now(), LocalTime.of(8, 0, 0)).format(formatter);
        String end = LocalDateTime.of(LocalDate.now(), LocalTime.of(8, 0, 0)).minusDays(1).format(formatter);
        System.out.println("start" + start);
        System.out.println("end" + end);
        //JDK8 LocalDateTime 获取当前时间 严格按照ISO 8601格式
        LocalDate now = LocalDateTime.ofInstant(new Date().toInstant(), ZoneId.systemDefault()).toLocalDate();
        System.out.println("now" + now);
        String eight = LocalTime.of(8, 0, 0).toString();
        System.out.println("eight" + eight);
        //string 转 localDateTime 指定格式
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DatePattern.NORM_DATETIME_PATTERN);
        String s = "2022-05-10 08:00:00";
        LocalDateTime stringToLocalDateTime = LocalDateTime.parse(s, formatter);
        System.out.println("stringToLocalDateTime" + stringToLocalDateTime);
        //JDK8 LocalDateTime 获取指定时间
        LocalDateTime localDateTimeEight = LocalDateTime.of(LocalDate.now(), LocalTime.of(8, 0, 0));
        LocalDateTime localDateTime2019 = LocalDateTime.of(2019, 11, 19, 8, 15, 0);
        //比较前后
        System.out.println(localDateTimeEight.isAfter(localDateTime2019));
        System.out.println(stringToLocalDateTime.isAfter(localDateTime2019));
        //时间加减
        LocalDateTime yesterdayEight = localDateTimeEight.minusDays(1L);
        System.out.println("yesterdayEight" + yesterdayEight);
        System.out.println("格式化localDateTimeEight" + localDateTimeEight.format(formatter));
        System.out.println("格式化yesterdayEight" + yesterdayEight.format(formatter));


        LocalDate today1 = LocalDate.now();
        LocalTime eight1 = LocalTime.of(8, 0, 0);
        LocalDateTime todayEight = LocalDateTime.of(today1, eight1);
        String start2 = todayEight.minusDays(1).format(formatter);
        String end2 = todayEight.format(formatter);
        System.out.println("start" + start2);
        System.out.println("end" + end2);
    }
}
