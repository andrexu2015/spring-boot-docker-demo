package com.example.springbootdockertest.controller.test;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

/**
 * sku test
 *
 * @Author liguangcheng
 * @Date 2021/10/13 3:01 下午
 * @Vision 1.0
 **/
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class SkuTest {
    @JsonProperty("package1")
    private Sku sku;
    @JSONField(name = "package")
    private PackageModel packageModel;
}
