package com.example.springbootdockertest.controller.test;

/**
 * JVM test
 *
 * @Author liguangcheng
 * @Date 2021/11/18 3:37 下午
 * @Vision 1.0
 **/
public class JavaHeapTest {
    public final static int OUTOFMEMORY = 200000000;

    private String oom;

    private int length;

    StringBuffer tempOOM = new StringBuffer();

    public JavaHeapTest(int leng) {
        this.length = leng;

        int i = 0;
        while (i < leng) {
            i++;
            try {
                tempOOM.append("a");
            } catch (OutOfMemoryError e) {
                e.printStackTrace();
                break;
            }
        }
        this.oom = tempOOM.toString();

    }

    public String getOom() {
        return oom;
    }

    public int getLength() {
        return length;
    }

    public static void main(String[] args) {
        // JavaHeapTest javaHeapTest = new JavaHeapTest(OUTOFMEMORY);
        // System.out.println(javaHeapTest.getOom().length());
        // method(null);

    }

    public static void method(String param) {
        switch (param) {
            // 肯定不是进入这里
            case "sth":
                System.out.println("it's sth");
                break;
            // 也不是进入这里
            case "null":
                System.out.println("it's null");
                break;
            // 也不是进入这里
            default:
                System.out.println("default");
        }
    }


}
