package com.example.springbootdockertest.controller.test;

import cn.hutool.core.util.CharsetUtil;
import cn.hutool.crypto.SecureUtil;
import cn.hutool.crypto.symmetric.SymmetricAlgorithm;
import cn.hutool.crypto.symmetric.SymmetricCrypto;
import org.springframework.util.Base64Utils;
import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

import java.nio.charset.StandardCharsets;

/**
 * 加密解密测试
 *
 * @Author liguangcheng
 * @Date 2022/6/3 08:45
 * @Vision 1.0
 **/
public class SecureTest {
    public static void main(String[] args) {
        // String format = DateUtil.format(new Date(), DatePattern.PURE_DATETIME_PATTERN);
        // String content = format + "reebelo-au.myshopify.com" + StrUtil.DOUBLE_DOT + "999666";
        String content = "hello world hello worldhello worldhello worldhello worldhello worldhello worldhello world";
        System.out.println("原字符串==>" + content);
        //使用spring Base64Utils加密解密
        springSecureStr(content);
        //使用hutool加密解密
        hutoolSecureStr(content);
    }

    private static void hutoolSecureStr(String content) {
        //随机生成密钥
        byte[] key = SecureUtil.generateKey(SymmetricAlgorithm.AES.getValue()).getEncoded();
        //保存密钥字符串
        String keyStr = new BASE64Encoder().encodeBuffer(key);
        //构建
        SymmetricCrypto aes = new SymmetricCrypto(SymmetricAlgorithm.AES, key);
        //加密为16进制表示
        String token = aes.encryptHex(content);
        byte[] bytes1 = new byte[0];
        try {
            bytes1 = new BASE64Decoder().decodeBuffer(keyStr);
        } catch (Exception e) {

        }
        SymmetricCrypto des = new SymmetricCrypto(SymmetricAlgorithm.AES, bytes1);
        //解密为字符串
        String decryptStr = des.decryptStr(token, CharsetUtil.CHARSET_UTF_8);
        System.out.println("hutool解密秘钥=>" + keyStr);
        System.out.println("hutool解密后字符串=>" + decryptStr);
    }

    private static void springSecureStr(String content) {
        //加密
        String encodeStr = new String(Base64Utils.encode(content.getBytes(StandardCharsets.UTF_8)));
        System.out.println("spring加密后字符串=>" + encodeStr);
        //解密
        byte[] bytes = Base64Utils.decodeFromString(encodeStr);
        String decodeStr = new String(bytes);
        System.out.println("spring解密后字符串=>" + decodeStr);
    }
}
