package com.example.springbootdockertest.controller.test;

import com.example.springbootdockertest.controller.pay.WxMssDTO;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * strean测试
 *
 * @Author liguangcheng
 * @Date 2022/6/3 09:31
 * @Vision 1.0
 **/
public class StreamTest {
    public static void main(String[] args) {
        List<String> testList = new ArrayList();
        testList.add("d");
        testList.add("c");
        List<String> list1 = new ArrayList();
        list1.add("a");
        list1.add("b");

        WxMssDTO wxMssDTO = new WxMssDTO();
        wxMssDTO.setPage("2");
        wxMssDTO.setTouser("4");
        wxMssDTO.setTestList(testList);
        WxMssDTO wxMssDTO1 = new WxMssDTO();
        wxMssDTO1.setPage("3");
        wxMssDTO1.setTouser("5");
        wxMssDTO1.setTestList(list1);
        List<WxMssDTO> wxMssDTOList = new ArrayList();
        wxMssDTOList.add(wxMssDTO1);
        wxMssDTOList.add(wxMssDTO);

        //把wxMssDTOList中每个WxMssDTO中的test集合中的元素放在一起
        List<String> testLists = wxMssDTOList.stream().map(WxMssDTO::getTestList).flatMap(Collection::stream).collect(Collectors.toList());
        System.out.println("test集合中的元素==>" + testLists);

        //把wxMssDTOList中每个WxMssDTO中的Page和touser放在一起
        List<String> pageAndTouserList = wxMssDTOList.stream().flatMap(s -> {
            String page = s.getPage();
            String touser = s.getTouser();
            List<String> strings = new ArrayList();
            strings.add(page);
            strings.add(touser);
            // 将每个元素转换成一个stream
            Stream<String> stream = strings.stream();
            return stream;
        }).collect(Collectors.toList());
        System.out.println("Page和touser==>" + pageAndTouserList);
    }
}
