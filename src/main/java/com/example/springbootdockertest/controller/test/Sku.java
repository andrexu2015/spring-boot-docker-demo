package com.example.springbootdockertest.controller.test;

import lombok.*;

/**
 * test sku
 *
 * @Author liguangcheng
 * @Date 2021/10/13 3:01 下午
 * @Vision 1.0
 **/
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class Sku {
    private String a;
}
