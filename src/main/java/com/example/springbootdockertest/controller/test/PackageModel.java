package com.example.springbootdockertest.controller.test;

import lombok.*;

import java.math.BigDecimal;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
/**
 * 包裹信息
 */
public class PackageModel {

    /**
     * 参考编号（客户原单号）
     */
    private String reference_number;

    /**
     * 申报价值
     */
    private BigDecimal declared_value;

    /**
     * 代收款
     */
    private BigDecimal cod_value;

    /**
     * 申报币别
     */
    private String declared_value_currency;

    /**
     * 代收币别
     */
    private String cod_value_currency;

    /**
     * 高
     */
    private String height;

    /**
     * 长
     */
    private String length;

    /**
     * 宽
     */
    private String width;

    /**
     * 实重, 单位 : 克
     */
    private BigDecimal actual_weight;

    /**
     * ?
     */
    private String shipment_term = "DDP";

    /**
     * 支付方式
     */
    private String payment_method = "PP";

    /**
     * 货物类型
     */
    private String shipment_type;

    /**
     * 订单信息.转单号码
     */
    private String tracking_number;

    /**
     * 分拣码
     */
    private String sort_code;

    /**
     * 保险金额
     */
    private BigDecimal insurance_value;

    /**
     * 保险币别
     */
    private String insurance_currency;

    /**
     * 同一包裹下所有产品销售总金额(总价值)
     */
    private BigDecimal cargo_value;
    /**
     * 平台收取的运费
     */
    private BigDecimal shipping_fee = new BigDecimal(0);
    /**
     * 件数
     */
    private Integer number_of_package = 1;
}
