package com.example.springbootdockertest.service;

import com.example.springbootdockertest.entity.DO.PaymentInfoDo;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author crush
 * @since 2022-04-03
 */
public interface PaymentInfoService extends IService<PaymentInfoDo> {

}
