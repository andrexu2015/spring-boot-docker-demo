package com.example.springbootdockertest.service.impl;

import com.example.springbootdockertest.service.DubboServiceTest;

/**
 * dubbo测试impl
 *
 * @Author liguangcheng
 * @Date 2021/10/6 2:43 下午
 * @Vision 1.0
 **/
// @DubboService(interfaceClass = DubboServiceTest.class, version = "1.0.0")
public class DubboServiceTestImpl implements DubboServiceTest {
    // @Override
    // public String dubboTest() {
    //     return "hello dubbo";
    // }
}
