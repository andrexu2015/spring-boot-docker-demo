package com.example.springbootdockertest.service.impl;

import com.example.springbootdockertest.entity.DO.PaymentInfoDo;
import com.example.springbootdockertest.mapper.PaymentInfoMapper;
import com.example.springbootdockertest.service.PaymentInfoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author crush
 * @since 2022-04-03
 */
@Service
public class PaymentInfoServiceImpl extends ServiceImpl<PaymentInfoMapper, PaymentInfoDo> implements PaymentInfoService {

}
