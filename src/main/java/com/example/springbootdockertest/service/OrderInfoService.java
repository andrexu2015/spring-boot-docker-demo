package com.example.springbootdockertest.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.springbootdockertest.entity.DO.OrderInfoDo;

import java.sql.SQLException;
import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author crush
 * @since 2022-04-03
 */
public interface OrderInfoService extends IService<OrderInfoDo> {

    OrderInfoDo getOrder();

    String threadOrder(List<OrderInfoDo> employeeDOList);

    String threadOrderTrue(List<OrderInfoDo> employeeDOList) throws SQLException;

    List<OrderInfoDo> getOrderList();
}
