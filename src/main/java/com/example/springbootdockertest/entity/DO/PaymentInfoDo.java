package com.example.springbootdockertest.entity.DO;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.time.LocalDateTime;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 
 * </p>
 *
 * @author crush
 * @since 2022-04-03
 */
@Getter
@Setter
@TableName("t_payment_info")
public class PaymentInfoDo implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 支付记录id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 商户订单编号
     */
    @TableField("order_no")
    private String orderNo;

    /**
     * 支付系统交易编号
     */
    @TableField("transaction_id")
    private String transactionId;

    /**
     * 支付类型
     */
    @TableField("payment_type")
    private String paymentType;

    /**
     * 交易类型
     */
    @TableField("trade_type")
    private String tradeType;

    /**
     * 交易状态
     */
    @TableField("trade_state")
    private String tradeState;

    /**
     * 支付金额(分)
     */
    @TableField("payer_total")
    private Integer payerTotal;

    /**
     * 通知参数
     */
    @TableField("content")
    private String content;

    /**
     * 创建时间
     */
    @TableField("create_time")
    private LocalDateTime createTime;

    /**
     * 更新时间
     */
    @TableField("update_time")
    private LocalDateTime updateTime;


}
