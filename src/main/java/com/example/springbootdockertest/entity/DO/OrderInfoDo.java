package com.example.springbootdockertest.entity.DO;

import com.alibaba.excel.annotation.ExcelIgnore;
import com.alibaba.excel.annotation.ExcelProperty;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 *
 * </p>
 *
 * @author crush
 * @since 2022-04-03
 */
@Getter
@Setter
@TableName("t_order_info")
public class OrderInfoDo implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 订单id
     */
    @ExcelIgnore
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 订单标题
     */
    @TableField("title")
    @ExcelProperty(value = "title", order = 1)
    private String title;

    /**
     * 商户订单编号
     */
    @TableField("order_no")
    @ExcelProperty(value = "订单号", order = 1)
    private String orderNo;

    /**
     * 用户id
     */
    @TableField("user_id")
    @ExcelProperty(value = "user_id", order = 1)
    private Long userId;

    /**
     * 支付产品id
     */
    @ExcelIgnore
    @TableField("product_id")
    private Long productId;

    /**
     * 订单金额(分)
     */
    @ExcelIgnore
    @TableField("total_fee")
    private Integer totalFee;

    /**
     * 订单二维码连接
     */
    @TableField("code_url")
    @ExcelIgnore
    private String codeUrl;

    /**
     * 订单状态
     */
    @TableField("order_status")
    @ExcelProperty(value = "订单号", order = 1)
    private String orderStatus;

    /**
     * 创建时间
     */
    @TableField("create_time")
    @ExcelIgnore
    private LocalDateTime createTime;

    /**
     * 更新时间
     */
    @TableField("update_time")
    @ExcelIgnore
    private LocalDateTime updateTime;


}
