package com.example.springbootdockertest.entity.DO;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

/**
 * MailSendDto
 *
 * @author crush
 * @version 1.0
 * @date 2022-04-22 11:10
 **/
@Getter
@Setter
public class MailSendDto {

  /**
   * 对方用户名
   */
  private String userName;


  /**
   * 对方地址
   */
  private String address;

  /**
   * 标题
   */
  private String title;

  /**
   * 要发送的内容
   */
  private String data;


  /**
   * 抄送地址
   */
  private List<String> ccAddressList = new ArrayList<>();

}
