package com.example.springbootdockertest;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
// @EnableDubbo
// @EnableAsync
@MapperScan("com.example.springbootdockertest.mapper")
@EnableTransactionManagement
public class SpringBootDockerTestApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootDockerTestApplication.class, args);
    }

}
